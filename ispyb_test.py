# -*- coding: utf-8 -*-
from requests import Session
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.transports import Transport
import json

#host = "http://localhost:8080/ispyb/ispyb-ws/ispybWS/"
user = "opd29"
password = 
beamline = "bm29"

proposal = ("opd", "29")

session = Session()
session.auth = HTTPBasicAuth(user, password)

_shipping_client = Client(
    host + "ToolsForShippingWebService?wsdl",
    transport=Transport(session=session),
)

_collection_client = Client(
    host + "ToolsForCollectionWebService?wsdl",
    transport=Transport(session=session),
)

_biosaxs_client = Client(
    host + "ToolsForBiosaxsWebService?wsdl",
    transport=Transport(session=session),
)

_generic_biosaxs_client = Client(
    host + "GenericSampleChangerBiosaxsWebService?wsdl",
    transport=Transport(session=session),
)

def find_sessions(code, number):
    return _collection_client.service.findSessionsByProposalAndBeamLine(
        code + number, beamline
    )

def find_proposal(code, number):
    return _shipping_client.service.findProposal(code, number)

# This return the Sample and Buffer Parameter to be pass to store_sc_experiment
def get_test_samples():
    data = {
"samples":[{'plate': '2', 'enable': True, 'macromolecule': '', 'title': 'P2-4:10', 'transmission': 100.0, 'well': '10', 'recuperate': False, 'SEUtemperature': 4.0, 'viscosity': 'Low', 'flow': False, 'comments': 'water buffer', 'volume': 10, 'buffername': 'bwater', 'code': 'water', 'typen': 0, 'waittime': 0.0, 'concentration': 0.0, 'type': 'Buffer', 'row': 4}, {'plate': '2', 'enable': True, 'macromolecule': 'WATER', 'buffer': '', 'flow': True, 'recuperate': False, 'viscosity': 'Low', 'typen': 1, 'volume': 50, 'buffername': 'bwater', 'code': 'water', 'concentration': 1.0, 'row': 4, 'waittime': 0.0, 'title': 'P2-4:11', 'transmission': 100.0, 'SEUtemperature': 20.0, 'well': '11', 'comments': '[1] water', 'type': 'Sample'}]

    }
    return data

# I run This to create a experiment as TEMPLATE
def store_sc_experiment(name):
    # import pdb; pdb.set_trace()
    pcode, pnumber = "opd", "29" 
    storage_temperature = 20 # samples[0]['storage_temperature']
    buffer_mode = "BEFORE"# samples[0]['buffer_mode']
    extra_flow_time = 0 #samples[0]['extra_flow_time']
    experiment_name = name


    _biosaxs_client.service.createExperiment(
        pcode,
        pnumber,
        json.dumps([{'plate': '2', 'enable': True, 'macromolecule': '', 'title': 'P2-4:10', 'transmission': 100.0, 'well': '10', 'recuperate': False, 'SEUtemperature': 4.0, 'viscosity': 'Low', 'flow': False, 'comments': 'water buffer', 'volume': 10, 'buffername': 'bwater', 'code': 'water', 'typen': 0, 'waittime': 0.0, 'concentration': 0.0, 'type': 'Buffer', 'row': 4}, {'plate': '2', 'enable': True, 'macromolecule': 'WATER', 'buffer': '', 'flow': True, 'recuperate': False, 'viscosity': 'Low', 'typen': 1, 'volume': 50, 'buffername': 'bwater', 'code': 'water', 'concentration': 1.0, 'row': 4, 'waittime': 0.0, 'title': 'P2-4:11', 'transmission': 100.0, 'SEUtemperature': 20.0, 'well': '11', 'comments': '[1] water', 'type': 'Sample'}]
),
        storage_temperature,
        buffer_mode,
        extra_flow_time,
        "STATIC", 
        "",
        experiment_name)
    # import pdb; pdb.set_trace()


# Print All stored TEMPLATE Experiment 
def list_experiments():
    pcode, pnumber = "opd", "29"
    experiments = _biosaxs_client.service.findExperimentByProposalCode(pcode, pnumber)
    print(experiments)
    return experiments

# run those two
store_sc_experiment('Test_from_get_test_samples')  # this return nothing 
list_experiments() # I ran this to see if the experiment was added to the list
